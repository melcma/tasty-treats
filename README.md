A test application,

features:  
A form capturing name, email, subscribe boolean and message  
Frontend validation and server-side form validation  
Saving entries to file and reading from it  
A bot spam prevention

Frontend is static html files and backend is written in Node.js and Express server, it uses express-validator to validate forms on a backend  
Client and Server are in separated docker containers  

run "yarn dev" or "npm run dev" locally
